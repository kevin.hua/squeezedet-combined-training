#!/bin/bash

export ROOT_DIR="/mnt/ssd2/od/public-roads/dummy"
export FOLDER="2019-02-23"
#export CKPT="/mnt/ssd2/od/philip/log/noaug/train/model.ckpt-54500"
export CKPT="/mnt/ssd2/od/kevin/log/train/model.ckpt-59000"
#export CKPT="/mnt/ssd2/od/kevin/od_squeezedet/data/model_checkpoints/squeezeDet/model.ckpt-43000"
export GPU="0"

python ./src/demo_citti.py \
  --mode=image \
  --input_path="$ROOT_DIR/$FOLDER/" \
  --output_path="$ROOT_DIR/$FOLDER/det_kitti/" \
  --checkpoint=$CKPT \
  --demo_net=squeezeDet \
  --gpu=$GPU \
  --save_video=False
