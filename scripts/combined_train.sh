#!/bin/bash

export GPUID=1
export NET="scale"
export TRAIN_DIR="/mnt/ssd2/od/kevin/log_combined/old_anchor_boxes/"
export DATASET="COMBINED"

case "$NET" in 
"squeezeDet")
export PRETRAINED_MODEL_PATH="./data/SqueezeNet/squeezenet_v1.1.pkl"
;;
"squeezeDet+")
export PRETRAINED_MODEL_PATH="./data/SqueezeNet/squeezenet_v1.0_SR_0.750.pkl"
;;
"resnet50")
export PRETRAINED_MODEL_PATH="./data/ResNet/ResNet-50-weights.pkl"
;;
"vgg16")
export PRETRAINED_MODEL_PATH="./data/VGG16/VGG_ILSVRC_16_layers_weights.pkl"
;;
"scale")
export PRETRAINED_MODEL_PATH="./data/SqueezeNet/squeezenet_v1.1.pkl"
;;
*)
echo "net architecture not supported."
exit 0
;;
esac


python ./src/combined_train.py \
  --dataset=$DATASET \
  --pretrained_model_path=$PRETRAINED_MODEL_PATH \
  --data_path=/mnt/ssd2/od/datasets/mapillary_kitti \
  --data_path_0=/mnt/ssd2/od/datasets/SCALE \
  --data_path_8=/mnt/ssd2/od/datasets/cityscape_kitti \
  --image_set=train \
  --train_dir="$TRAIN_DIR/train" \
  --net=$NET \
  --summary_step=100 \
  --checkpoint_step=1000 \
  --max_steps=15000
