#!/bin/bash

export GPUID=""
export NET="squeezeDet"
export EVAL_DIR="/mnt/ssd2/od/kevin/mapillary_log"
export DATASET="KITTI"
export IMAGE_SET="val"


# =========================================================================== #
# command for squeezeDet:
# =========================================================================== #
python ./src/eval.py \
  --dataset=$DATASET \
  --data_path=/mnt/ssd2/od/mapillary/mapillary_kitti_person/ \
  --cityscape_data_path=/mnt/ssd2/od/cityscape/data/ \
  --image_set=$IMAGE_SET \
  --eval_dir="$EVAL_DIR/$IMAGE_SET" \
  --checkpoint_path="$EVAL_DIR/train" \
  --net=$NET \
  --gpu=$GPUID
