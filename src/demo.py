# Author: Bichen Wu (bichen@berkeley.edu) 08/25/2016

"""SqueezeDet Demo.

In image detection mode, for a given image, detect objects and draw bounding
boxes around them. In video detection mode, perform real-time detection on the
video stream.
"""

import sys
import os
BAD_CV='/opt/ros/kinetic/lib/python2.7/dist-packages'
if BAD_CV in sys.path:
    sys.path.remove(BAD_CV)
if os.getcwd() not in sys.path:
    sys.path.append(os.getcwd())
import cv2
import time
import glob

import numpy as np
import tensorflow as tf

from src.config import *
from src.train import _draw_box
from src.nets import *

FLAGS = tf.app.flags.FLAGS

tf.app.flags.DEFINE_string(
    'mode', 'image', """'image' or 'video'.""")
tf.app.flags.DEFINE_string(
    'checkpoint', '/mnt/ssd2/od/kevin/mapillary_log/train/model.ckpt-69000',
    """Path to the model parameter file.""")
tf.app.flags.DEFINE_string(
    'input_path', '/mnt/ssd2/od/public-roads/dummy/2019-02-23/*.jpg',
    """Input image or video to be detected. Can process glob input such as """
    """./data/00000*.png.""")
tf.app.flags.DEFINE_string(
    'out_dir', './data/out/', """Directory to dump output image or video.""")
tf.app.flags.DEFINE_string(
    'demo_net', 'squeezeDet', """Neural net architecture.""")
tf.app.flags.DEFINE_string(
	'save_video', 'False', """Save to output to a video""")


def resize_image(image, mc):
    height, width, _ = image.shape
    if height == mc.IMAGE_HEIGHT and width == mc.IMAGE_WIDTH:
        return image
     
    if mc.IMAGE_WIDTH * 1.0 / width < mc.IMAGE_HEIGHT * 1.0 / height:
        resized_width = mc.IMAGE_WIDTH
        resized_height = int(height * mc.IMAGE_WIDTH / width)
        resized_im = cv2.resize(image, (resized_width, resized_height))
        extra_top = (mc.IMAGE_HEIGHT - resized_height) // 2
        extra_bottom = mc.IMAGE_HEIGHT - resized_height - extra_top
        im = cv2.copyMakeBorder(resized_im, extra_top, extra_bottom, 0, 0, cv2.BORDER_CONSTANT, 0)

    else:
        resized_height = mc.IMAGE_HEIGHT
        resized_width = int(width * mc.IMAGE_HEIGHT / height)
        resized_im = cv2.resize(image, (resized_width, resized_height))
        extra_left = (mc.IMAGE_WIDTH - resized_width) // 2
        extra_right = mc.IMAGE_WIDTH - resized_width - extra_left
        im = cv2.copyMakeBorder(resized_im, 0, 0, extra_left, extra_right, cv2.BORDER_CONSTANT, 0)
    return im

def cropresize(image, mc):
    h_offset = 492
    h_blackfly = 753
    input_image = image[h_offset:h_offset + h_blackfly + 1, :, :]
    W = mc.IMAGE_WIDTH
    H = mc.IMAGE_HEIGHT
    w_orig = input_image.shape[1]
    h_orig = input_image.shape[0]
    input_image = cv2.resize(input_image, (W, H))
    scale_w = float(w_orig) / float(W)
    scale_h = float(h_orig) / float(H)    

    return input_image, scale_w, scale_h, h_offset

def image_demo():
  """Detect image."""
  print("Image_demo")
  assert FLAGS.demo_net == 'squeezeDet' or FLAGS.demo_net == 'squeezeDet+', \
      'Selected nueral net architecture not supported: {}'.format(FLAGS.demo_net)
  print("0")
  os.environ['CUDA_VISIBLE_DEVICES'] = "0"
    
  with tf.Graph().as_default() as g:
    # Load model
    if FLAGS.demo_net == 'squeezeDet':
      mc = kitti_squeezeDet_config()
      mc.BATCH_SIZE = 1
      # model parameters will be restored from checkpoint
      mc.LOAD_PRETRAINED_MODEL = False
      model = SqueezeDet(mc, 0)
    elif FLAGS.demo_net == 'squeezeDet+':
      mc = kitti_squeezeDetPlus_config()
      mc.BATCH_SIZE = 1
      mc.LOAD_PRETRAINED_MODEL = False
      model = SqueezeDetPlus(mc, FLAGS.gpu)

    saver = tf.train.Saver(model.model_params)
    #if FLAGS.save_video.lower() == 'true':
    #    videowriter = cv2.VideoWriter(out_file_name, fourcc, 1.0, (1224, 1024), True)
    #    #videowriter = cv2.VideoWriter(out_file_name, fourcc, 1.0, (mc.IMAGE_WIDTH, mc.IMAGE_HEIGHT), True)
    print(1)
    with tf.Session(config=tf.ConfigProto(allow_soft_placement=True)) as sess:
      saver.restore(sess, FLAGS.checkpoint)
      all_files = sorted(glob.glob(FLAGS.input_path + '/*.jpg'))
      times = {}
      print(all_files)
      for f in glob.iglob(FLAGS.input_path):
        print(f)
        t_start = time.time() 
        im = cv2.imread(f)
        im = im.astype(np.float32, copy=False)
        im, scale_w, scale_h, h_offset = cropresize(im, mc)
        input_image = im - mc.BGR_MEANS

        t_reshape = time.time()
        times['reshape']= t_reshape - t_start  

        # Detect
        det_boxes, det_probs, det_class = sess.run(
            [model.det_boxes, model.det_probs, model.det_class],
            feed_dict={model.image_input:[input_image]})

        t_detect = time.time()
        times['detect']= t_detect - t_reshape       

        # Filter
        final_boxes, final_probs, final_class = model.filter_prediction(
            det_boxes[0], det_probs[0], det_class[0])

        keep_idx    = [idx for idx in range(len(final_probs)) \
                          if final_probs[idx] > mc.PLOT_PROB_THRESH]
        final_boxes = [final_boxes[idx] for idx in keep_idx]
        final_probs = [final_probs[idx] for idx in keep_idx]
        final_class = [final_class[idx] for idx in keep_idx]
        
        t_filter = time.time()
        times['filter']= t_filter - t_detect
        
        
        # for box in final_boxes:
        #     box[0] = scale_w * box[0]
        #     box[1] = scale_h * box[1] + h_offset
        #     box[2] = scale_w * box[2]
        #     box[3] = scale_h * box[3] 
        

        # TODO(bichen): move this color dict to configuration file
        cls2clr = {
            'car': (255, 191, 0),
            'cyclist': (0, 191, 255),
            'pedestrian':(255, 0, 191)
        }

        # Draw boxes
        _draw_box(
            im, final_boxes,
            [mc.CLASS_NAMES[idx]+': (%.2f)'% prob \
                for idx, prob in zip(final_class, final_probs)],
            cdict=cls2clr,
            font_scale = 0.5
        )
        #print(final_boxes)
        t_draw = time.time()
        times['draw']= t_draw - t_filter
		
        times['total']= time.time() - t_start
        time_str = 'Total time: {:.4f}, load&reshape time: {:.4f} detection time: {:.4f}, filter time: '\
                   '{:.4f}'. \
            format(times['total'], times['reshape'], times['detect'], times['filter'])

        print (time_str)		
        #fps = int(1.0 / times['total'] + 0.5)
        #cv2.putText(im, "FPS: " + str(fps), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2)

        file_name = os.path.split(f)[1]
        out_file_name = os.path.join(FLAGS.out_dir, 'out_'+file_name)
        cv2.imwrite(out_file_name, im)
        print ('Image detection output saved to {}'.format(out_file_name))
        #out_im = im.astype(np.uint8, copy=False)
        #out_im = cv2.resize(out_im, (mc.IMAGE_WIDTH, mc.IMAGE_HEIGHT))
        #cv2.namedWindow("det_feed", cv2.WINDOW_NORMAL)
        #cv2.imshow("det_feed", out_im)
        #videowriter.write(out_im)
        #cv2.waitKey(1)

    #videowriter.release()

        # print ('Image detection output saved to {}'.format(out_file_name))

def main(argv=None):
  print("start")
  if not tf.gfile.Exists(FLAGS.out_dir):
    tf.gfile.MakeDirs(FLAGS.out_dir)
  if FLAGS.mode == 'image':
    image_demo()
  else:
    video_demo()

if __name__ == '__main__':
    tf.app.run()
