# Author: Bichen Wu (bichen@berkeley.edu) 08/25/2016

"""SqueezeDet Demo.

In image detection mode, for a given image, detect objects and draw bounding
boxes around them. In video detection mode, perform real-time detection on the
video stream.
"""

import sys
import os
BAD_CV='/opt/ros/kinetic/lib/python2.7/dist-packages'
if BAD_CV in sys.path:
    sys.path.remove(BAD_CV)
if os.getcwd() not in sys.path:
    sys.path.append(os.getcwd())
import cv2
import time
import glob

import numpy as np
import tensorflow as tf

from src.utils.transform import ImageTransform
from src.config import *
from src.train import _draw_box
from src.nets import *

FLAGS = tf.app.flags.FLAGS

tf.app.flags.DEFINE_string(
    'mode', 'image', """'image' or 'video'.""")
tf.app.flags.DEFINE_string(
    'checkpoint', '/mnt/ssd2/od/kevin/od_squeezedet/data/model_checkpoints/squeezeDet/model.ckpt-87000',
    """Path to the model parameter file.""")
tf.app.flags.DEFINE_string(
    'input_path', './data/sample.png',
    """Input image or video to be detected. Can process glob input such as """
    """./data/00000*.png.""")
tf.app.flags.DEFINE_string(
    'out_dir', './data/out/', """Directory to dump output image or video.""")
tf.app.flags.DEFINE_string(
    'demo_net', 'squeezeDet', """Neural net architecture.""")
tf.app.flags.DEFINE_string(
	'save_video', 'False', """Save to output to a video""")
print(FLAGS.checkpoint)
def resize_image(image, mc):
    # Resize with padding
    height, width, _ = image.shape
    if height == mc.IMAGE_HEIGHT and width == mc.IMAGE_WIDTH:
        return image
     
    if mc.IMAGE_WIDTH * 1.0 / width < mc.IMAGE_HEIGHT * 1.0 / height:
        resized_width = mc.IMAGE_WIDTH
        resized_height = int(height * mc.IMAGE_WIDTH / width)
        resized_im = cv2.resize(image, (resized_width, resized_height))
        extra_top = (mc.IMAGE_HEIGHT - resized_height) // 2
        extra_bottom = mc.IMAGE_HEIGHT - resized_height - extra_top
        im = cv2.copyMakeBorder(resized_im, extra_top, extra_bottom, 0, 0, cv2.BORDER_CONSTANT, 0)

    else:
        resized_height = mc.IMAGE_HEIGHT
        resized_width = int(width * mc.IMAGE_HEIGHT / height)
        resized_im = cv2.resize(image, (resized_width, resized_height))
        extra_left = (mc.IMAGE_WIDTH - resized_width) // 2
        extra_right = mc.IMAGE_WIDTH - resized_width - extra_left
        im = cv2.copyMakeBorder(resized_im, 0, 0, extra_left, extra_right, cv2.BORDER_CONSTANT, 0)
    return im

def resize(input_image, mc): 
    h_offset = 0
    W = mc.IMAGE_WIDTH
    H = mc.IMAGE_HEIGHT
    w_orig = input_image.shape[1]
    h_orig = input_image.shape[0]
    input_image = input_image.astype(np.uint8, copy=True)
    input_image = cv2.resize(input_image, (W, H))
    input_image = input_image.astype(np.float32, copy=False)
    scale_w = float(w_orig) / float(W)
    scale_h = float(h_orig) / float(H)    

    return input_image, scale_w, scale_h, h_offset

def cropresize(image, mc):
    h_offset = 492
    h_blackfly = 753
    input_image = image[h_offset:h_offset + h_blackfly + 1, :, :]
    W = mc.IMAGE_WIDTH
    H = mc.IMAGE_HEIGHT
    w_orig = input_image.shape[1]
    h_orig = input_image.shape[0]
    input_image = cv2.resize(input_image, (W, H))
    scale_w = float(w_orig) / float(W)
    scale_h = float(h_orig) / float(H)    

    return input_image, scale_w, scale_h, h_offset

def image_demo():
  """Detect image."""

  assert FLAGS.demo_net == 'squeezeDet' or FLAGS.demo_net == 'squeezeDet+', \
      'Selected nueral net architecture not supported: {}'.format(FLAGS.demo_net)
  
  os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu 
  if FLAGS.save_video.lower() == 'true':
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
    in_file_name = os.path.split(FLAGS.input_path)[1]
    out_file_name = os.path.join(FLAGS.input_path, 'det_coco.avi')
  print("0")
  tf.reset_default_graph() 
  with tf.Graph().as_default():
    # Load model
    if FLAGS.demo_net == 'squeezeDet':
      mc = coco_squeezeDet_config()
      mc.BATCH_SIZE = 1
      # model parameters will be restored from checkpoint
      mc.LOAD_PRETRAINED_MODEL = False
      model = SqueezeDet(mc)
      img_transform = ImageTransform(**mc.img_norm_cfg)
     
    saver = tf.train.Saver(model.model_params, reshape=True)
         
    if FLAGS.save_video.lower() == 'true':
        videowriter = cv2.VideoWriter(out_file_name, fourcc, 1.0, (1224, 1024), True)
        #videowriter = cv2.VideoWriter(out_file_name, fourcc, 1.0, (mc.IMAGE_WIDTH, mc.IMAGE_HEIGHT), True)
    
    with tf.Session(config=tf.ConfigProto(allow_soft_placement=True)) as sess:
      print("1")
      #sess.run(tf.local_variables_initializer())
      #loader = tf.train.import_meta_graph(FLAGS.checkpoint+'.meta')
      #loader.restore(sess, FLAGS.checkpoint)
      init = tf.local_variables_initializer()
      sess.run(init)
      
      print(saver.restore(sess, FLAGS.checkpoint))
      #print(model.model_params)
      #model.load_weights(FLAGS.checkpoint)
      all_files = sorted(glob.glob(FLAGS.input_path + '/*.jpg'))
      times = {}
      for f in iter(all_files):
        t_start = time.time() 
        original_im = cv2.imread(f)         
        input_image, scale_w, scale_h, h_offset = resize(original_im, mc) 
        input_image = input_image[:, :, ::-1] 
        input_image -= mc.BGR_MEANS
        #print(input_image.shape)
        t_reshape = time.time()
        times['reshape']= t_reshape - t_start
        #cv2.imshow("input_feed", input_image.astype(np.uint8))
        # Detect
        det_boxes, det_probs, det_class = sess.run(
            [model.det_boxes, model.det_probs, model.det_class],
            feed_dict={model.image_input:[input_image]})

        t_detect = time.time()
        times['detect']= t_detect - t_reshape       
        print(len(det_boxes[0]))

        # Filter
        final_boxes, final_probs, final_class = model.filter_prediction(
            det_boxes[0], det_probs[0], det_class[0])

        keep_idx    = [idx for idx in range(len(final_probs)) if final_probs[idx] > mc.PLOT_PROB_THRESH]
        final_boxes = [final_boxes[idx] for idx in keep_idx]
        final_probs = [final_probs[idx] for idx in keep_idx]
        final_class = [final_class[idx] for idx in keep_idx]
        
        t_filter = time.time()
        times['filter']= t_filter - t_detect
        
        
        for box in final_boxes:
            box[0] = scale_w * box[0]
            box[1] = scale_h * box[1] + h_offset
            box[2] = scale_w * box[2]
            box[3] = scale_h * box[3] 
        

        # TODO(bichen): move this color dict to configuration file
        cls2clr = {
            'car': (255, 191, 0),
            'cyclist': (0, 191, 255),
            'pedestrian':(255, 0, 191)
        }
        # Draw boxes
        _draw_box(
            original_im, final_boxes,
            [mc.CLASS_NAMES[idx]+': (%.2f)'% prob \
                for idx, prob in zip(final_class, final_probs)],
            cdict=cls2clr,
            font_scale = 0.5
        )
        print(len(final_boxes))
        t_draw = time.time()
        times['draw']= t_draw - t_filter
		
        times['total']= time.time() - t_start
        time_str = 'Total time: {:.4f}, load&reshape time: {:.4f} detection time: {:.4f}, filter time: '\
                   '{:.4f}'. \
            format(times['total'], times['reshape'], times['detect'], times['filter'])

        print (time_str)		
        #fps = int(1.0 / times['total'] + 0.5)
        #cv2.putText(im, "FPS: " + str(fps), (100,50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50,170,50), 2)
        file_name = os.path.split(f)[1]
        out_file_name = os.path.join(FLAGS.out_dir, 'out_'+file_name)
        out_im = original_im.astype(np.uint8, copy=False)
        out_im = cv2.resize(out_im, (1224, 1024))
        #out_im = im.astype(np.uint8, copy=False)
        
        if FLAGS.save_video.lower() == 'true':
            cv2.namedWindow("det_feed", cv2.WINDOW_NORMAL)
            #cv2.imshow("det_feed", out_im)
            videowriter.write(out_im)
        #cv2.waitKey(1)
        else:
            out_im = cv2.resize(out_im, (mc.IMAGE_WIDTH, mc.IMAGE_HEIGHT))
            cv2.imwrite(out_file_name, out_im)

    videowriter.release()

        # print ('Image detection output saved to {}'.format(out_file_name))

def main(argv=None):
  if not tf.gfile.Exists(FLAGS.out_dir):
    tf.gfile.MakeDirs(FLAGS.out_dir)
  if FLAGS.mode == 'image':
    image_demo()
  else:
    video_demo()

if __name__ == '__main__':
    tf.app.run()
