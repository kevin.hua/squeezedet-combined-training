# Author: Bichen Wu (bichen@berkeley.edu) 08/25/2016

"""Image data base class for kitti"""

import cv2
import os 
import numpy as np
import subprocess
import glob

from src.dataset.imdb import imdb
from src.utils.util import bbox_transform_inv, batch_iou

class combined(imdb):
  def __init__(self, image_set, data_path_list, mc):
    imdb.__init__(self, 'kitti_'+image_set, mc)
    self._image_set = image_set
    ############## EDITED #################
    # self._data_root_path = data_path
    # self._image_path = os.path.join(self._data_root_path, 'training', 'image_2')
    # self._label_path = os.path.join(self._data_root_path, 'training', 'label_2')
    self._data_path_list = data_path_list
    self._image_path_list = [os.path.join(d_path, 'training', 'image_2') for d_path in self._data_path_list]
    self._label_path_list = [os.path.join(d_path, 'training', 'label_2') for d_path in self._data_path_list]
    self._classes = self.mc.CLASS_NAMES
    self._class_to_idx = dict(zip(self.classes, range(self.num_classes)))
  

    # a list of string indices of images in the directory

    self._image_idx = self._load_image_set_idx() 
    # a dict of image_idx -> [[cx, cy, w, h, cls_idx]]. x,y,w,h are not divided by
    # the image width and height
    self._rois = self._load_kitti_annotation()

    ## batch reader ##
    self._perm_idx = None
    self._cur_idx = 0
    # TODO(bichen): add a random seed as parameter
    self._shuffle_image_idx()

    self._eval_tool = 'src/dataset/kitti-eval/cpp/evaluate_object'

  ############### EDITED ################
  # Edit the function such that _image_idx are in the form "data_path idx"
  def _load_image_set_idx(self):
    image_idx = []
    for d_path in self._data_path_list:
      samplef = next(glob.iglob(os.path.join(d_path, 'training', 'image_2', "*")))
      filename, ext = os.path.splitext(samplef)

      image_set_file = os.path.join(
          d_path, 'ImageSets', self._image_set+'.txt')
      assert os.path.exists(image_set_file), \
          'File does not exist: {}'.format(image_set_file)
      with open(image_set_file) as f:
        image_idx.extend([d_path + "---" + x.strip() + "---" + ext for x in f.readlines()])
      f.close()
    
    return image_idx

  ############### EDITED ################
  # Changed idx format
  def _image_path_at(self, idx):
    image_path = os.path.join(idx.split("---")[0], "training", 'image_2', idx.split("---")[1]+idx.split("---")[2])
    assert os.path.exists(image_path), \
        'Image does not exist: {}'.format(image_path)
    return image_path

  ############## EDITED ################
  # Changed idx format
  def _load_kitti_annotation(self):
    def _get_obj_level(obj):
      height = float(obj[7]) - float(obj[5]) + 1
      truncation = float(obj[1])
      occlusion = float(obj[2])
      if height >= 40 and truncation <= 0.15 and occlusion <= 0:
          return 1
      elif height >= 25 and truncation <= 0.3 and occlusion <= 1:
          return 2
      elif height >= 25 and truncation <= 0.5 and occlusion <= 2:
          return 3
      else:
          return 4

    idx2annotation = {}
    for idx in self._image_idx:
      label_filename = os.path.join(idx.split("---")[0], "training", 'label_2', idx.split("---")[1]+'.txt')
      with open(label_filename, 'r') as f:
        lines = f.readlines()
      f.close()
      bboxes = []
      for line in lines:
        obj = line.strip().split(' ')
        try:
          cls = self._class_to_idx[obj[0].lower().strip()]
          if cls == 0:
            continue
          if cls == 2:
            cls = 1
        except:
          continue

        if self.mc.EXCLUDE_HARD_EXAMPLES and _get_obj_level(obj) > 3:
          continue
        xmin = float(obj[4])
        ymin = float(obj[5])
        xmax = float(obj[6])
        ymax = float(obj[7])

        # if ymax < top_crop or ymin > bot_crop:  # BB is outside cropped area
        #     continue
        # ymin = max(ymin, top_crop)
        # ymax = min(ymax, bot_crop)- ymin
       


        assert xmin >= 0.0 and xmin <= xmax, \
            'Invalid bounding box x-coord xmin {} or xmax {} at {}.txt' \
                .format(xmin, xmax, index)
        assert ymin >= 0.0 and ymin <= ymax, \
            'Invalid bounding box y-coord ymin {} or ymax {} at {}.txt' \
                .format(ymin, ymax, index)
        x, y, w, h = bbox_transform_inv([xmin, ymin, xmax, ymax])
        bboxes.append([x, y, w, h, cls])

      idx2annotation[idx] = bboxes

    return idx2annotation

  
  def evaluate_detections(self, eval_dir, global_step, all_boxes):
    """Evaluate detection results.
    Args:
      eval_dir: directory to write evaluation logs
      global_step: step of the checkpoint
      all_boxes: all_boxes[cls][image] = N x 5 arrays of 
        [xmin, ymin, xmax, ymax, score]
    Returns:
      aps: array of average precisions.
      names: class names corresponding to each ap
    """
    det_file_dir_list = []
    for i in range(len(self._data_path_list)):
      det_file_dir_list.append(os.path.join(
          eval_dir, 'detection_files_{:s}_{:i}'.format(global_step, i), 'data'))

      if not os.path.isdir(det_file_dir_list[i]):
        os.makedirs(det_file_dir_list[i])

    count = [0 for i in range(len(det_file_dir_list))]

    for im_idx, idx in enumerate(self._image_idx):
      # Index of the data_path
      d_path_ind = self._data_path_list.index(idx.split("---")[0])
      count[d_path_ind] += 1
      filename = os.path.join(det_file_dir_list[d_path_ind], idx.split("---")[1]+'.txt')
      with open(filename, 'wt') as f:
        for cls_idx, cls in enumerate(self._classes):
          dets = all_boxes[cls_idx][im_idx]
          for k in range(len(dets)):
            f.write(
                '{:s} -1 -1 0.0 {:.2f} {:.2f} {:.2f} {:.2f} 0.0 0.0 0.0 0.0 0.0 '
                '0.0 0.0 {:.3f}\n'.format(
                    cls.lower(), dets[k][0], dets[k][1], dets[k][2], dets[k][3],
                    dets[k][4])
            )

    all_aps = []
    all_names = []
    # Run command for each directory
    for i, det_file_dir in enumerate(det_file_dir_list):
      aps = []
      names = []
      cmd = self._eval_tool + ' ' \
            + os.path.join(self._data_path_list[i], 'training') + ' ' \
            + os.path.join(self._data_path_list[i], 'ImageSets',
                          self._image_set+'.txt') + ' ' \
            + os.path.dirname(det_file_dir) + ' ' + str(len(self._image_idx))

      print('Running: {}'.format(cmd))
      status = subprocess.call(cmd, shell=True)

      print("detfile dir is ", det_file_dir)
      for cls in self._classes:
        det_file_name = os.path.join(
            os.path.dirname(det_file_dir), 'stats_{:s}_ap.txt'.format(cls))
        if os.path.exists(det_file_name):
          with open(det_file_name, 'r') as f:
            lines = f.readlines()
          assert len(lines) == 3, \
              'Line number of {} should be 3'.format(det_file_name)

          aps.append(float(lines[0].split('=')[1].strip()))
          aps.append(float(lines[1].split('=')[1].strip()))
          aps.append(float(lines[2].split('=')[1].strip()))
        else:
          aps.extend([0.0, 0.0, 0.0])

        names.append(str(i)+"_"+cls+'_easy')
        names.append(str(i)+"_"+cls+'_medium')
        names.append(str(i)+"_"+cls+'_hard')
      all_aps.append(aps)
      all_names.append(names)
      
    return all_aps, all_names

  def do_detection_analysis_in_eval(self, eval_dir, global_step):
    

    all_stats = []
    all_ims = []
    ### Init def_file_dir_list
    det_dir_list = []
    for i in range(len(self._data_path_list)):
      det_dir_list.append(os.path.join(
          eval_dir, 'detection_files_{:s}_{:i}'.format(global_step, i)))

      if not os.path.isdir(det_dir_list[i]):
        os.makedirs(det_dir_list[i])

      det_data_dir = os.path.join(
          det_dir_list[i], 'data')
      det_error_dir = os.path.join(
          det_dir_list[i], 'error_analysis')

      if not os.path.exists(det_error_dir):
        os.makedirs(det_error_dir)
      
      det_error_file = os.path.join(det_error_dir, 'det_error_file.txt')

      stats = self.analyze_detections(det_data_dir, det_error_file)
      ims = self.visualize_detections(
          image_dir=self._image_path,
          image_format='.png',
          det_error_file=det_error_file,
          output_image_dir=det_error_dir,
          num_det_per_type=10
      )
      all_stats.append(stats)
      all_ims.append(ims)

    return all_stats, all_ims

  def analyze_detections(self, detection_file_dir, det_error_file):
    def _save_detection(f, idx, error_type, det, score):
      f.write(
          '{:s} {:s} {:.1f} {:.1f} {:.1f} {:.1f} {:s} {:.3f}\n'.format(
              idx, error_type,
              det[0]-det[2]/2., det[1]-det[3]/2.,
              det[0]+det[2]/2., det[1]+det[3]/2.,
              self._classes[int(det[4])], 
              score
          )
      )

    # load detections
    self._det_rois = {}
    for idx in self._image_idx:
      det_file_name = os.path.join(detection_file_dir, idx.split("---")[1]+'.txt')
      with open(det_file_name) as f:
        lines = f.readlines()
      f.close()
      bboxes = []
      for line in lines:
        obj = line.strip().split(' ')
        cls = self._class_to_idx[obj[0].lower().strip()]
        xmin = float(obj[4])
        ymin = float(obj[5])
        xmax = float(obj[6])
        ymax = float(obj[7])
        score = float(obj[-1])

        x, y, w, h = bbox_transform_inv([xmin, ymin, xmax, ymax])
        bboxes.append([x, y, w, h, cls, score])
      bboxes.sort(key=lambda x: x[-1], reverse=True)
      self._det_rois[idx] = bboxes

    # do error analysis
    num_objs = 0.
    num_dets = 0.
    num_correct = 0.
    num_loc_error = 0.
    num_cls_error = 0.
    num_bg_error = 0.
    num_repeated_error = 0.
    num_detected_obj = 0.

    with open(det_error_file, 'w') as f:
      for idx in self._image_idx:
        gt_bboxes = np.array(self._rois[idx])
        num_objs += len(gt_bboxes)
        detected = [False]*len(gt_bboxes)

        det_bboxes = self._det_rois[idx]
        if len(gt_bboxes) < 1:
          continue

        for i, det in enumerate(det_bboxes):
          if i < len(gt_bboxes):
            num_dets += 1
          ious = batch_iou(gt_bboxes[:, :4], det[:4])
          max_iou = np.max(ious)
          gt_idx = np.argmax(ious)
          if max_iou > 0.1:
            if gt_bboxes[gt_idx, 4] == det[4]:
              if max_iou >= 0.5:
                if i < len(gt_bboxes):
                  if not detected[gt_idx]:
                    num_correct += 1
                    detected[gt_idx] = True
                  else:
                    num_repeated_error += 1
              else:
                if i < len(gt_bboxes):
                  num_loc_error += 1
                  _save_detection(f, idx, 'loc', det, det[5])
            else:
              if i < len(gt_bboxes):
                num_cls_error += 1
                _save_detection(f, idx, 'cls', det, det[5])
          else:
            if i < len(gt_bboxes):
              num_bg_error += 1
              _save_detection(f, idx, 'bg', det, det[5])

        for i, gt in enumerate(gt_bboxes):
          if not detected[i]:
            _save_detection(f, idx, 'missed', gt, -1.0)
        num_detected_obj += sum(detected)
    f.close()

    print ('Detection Analysis:')
    print ('    Number of detections: {}'.format(num_dets))
    print ('    Number of objects: {}'.format(num_objs))
    print ('    Percentage of correct detections: {}'.format(
      num_correct/num_dets))
    print ('    Percentage of localization error: {}'.format(
      num_loc_error/num_dets))
    print ('    Percentage of classification error: {}'.format(
      num_cls_error/num_dets))
    print ('    Percentage of background error: {}'.format(
      num_bg_error/num_dets))
    print ('    Percentage of repeated detections: {}'.format(
      num_repeated_error/num_dets))
    print ('    Recall: {}'.format(
      num_detected_obj/num_objs))

    out = {}
    out['num of detections'] = num_dets
    out['num of objects'] = num_objs
    out['% correct detections'] = num_correct/num_dets
    out['% localization error'] = num_loc_error/num_dets
    out['% classification error'] = num_cls_error/num_dets
    out['% background error'] = num_bg_error/num_dets
    out['% repeated error'] = num_repeated_error/num_dets
    out['% recall'] = num_detected_obj/num_objs

    return out
