# --------------------------------------------------------
# Fast/er R-CNN
# Licensed under The MIT License [see LICENSE for details]
# Written by Ross Girshick
# --------------------------------------------------------

from src.dataset.imdb import imdb
from src.utils.util import imresize, batch_iou, bbox_transform_inv
from src.utils.transform import BboxTransform, ImageTransform
from src.dataset.extra_aug import ExtraAugmentation
import os.path as osp
import sys
import os
import numpy as np
import scipy.sparse
from six.moves import cPickle
import json
import uuid
import cv2
import mmcv
# COCO API
from pycocotools.coco import COCO
from pycocotools.cocoeval import COCOeval
from pycocotools import mask as COCOmask


def _filter_crowd_proposals(roidb, crowd_thresh):
    def xyxy_to_xywh(boxes):
        """Convert [x1 y1 x2 y2] box format to [x y w h] format."""
        return np.hstack((boxes[:, 0:2], boxes[:, 2:4] - boxes[:, 0:2] + 1))

    """
    Finds proposals that are inside crowd regions and marks them with
    overlap = -1 (for all gt rois), which means they will be excluded from
    training.
    """
    for ix, entry in enumerate(roidb):
        overlaps = entry['gt_overlaps'].toarray()
        crowd_inds = np.where(overlaps.max(axis=1) == -1)[0]
        non_gt_inds = np.where(entry['gt_classes'] == 0)[0]
        if len(crowd_inds) == 0 or len(non_gt_inds) == 0:
            continue
        iscrowd = [int(True) for _ in range(len(crowd_inds))]
        crowd_boxes = xyxy_to_xywh(entry['boxes'][crowd_inds, :])
        non_gt_boxes = xyxy_to_xywh(entry['boxes'][non_gt_inds, :])
        ious = COCOmask.iou(non_gt_boxes, crowd_boxes, iscrowd)
        bad_inds = np.where(ious.max(axis=1) > crowd_thresh)[0]
        overlaps[non_gt_inds[bad_inds], :] = -1
        roidb[ix]['gt_overlaps'] = scipy.sparse.csr_matrix(overlaps)
    return roidb


class coco(imdb):
    def __init__(self, image_set, data_path, mc, year='2017'):

        imdb.__init__(self, 'coco_' + year + '_' + image_set, mc)
        # COCO specific config options
        self.config = {'top_k' : 2000,
                       'use_salt' : True,
                       'cleanup' : True,
                       'crowd_thresh' : 0.7,
                       'min_size' : 2}
        # name, paths
        self._year = year
        self._image_set = image_set
        self._data_root_path = osp.join(data_path)
        self._img_prefix = mc.train_prefix if 'train' in image_set else mc.val_prefix
        # load COCO API, classes, class <-> id mappings
        self._COCO = COCO(self._get_ann_file())
        cats = self._COCO.loadCats(self._COCO.getCatIds())
        self._classes = tuple([c['name'] for c in cats])
        self._class_to_idx = dict(zip(self._classes, range(self.num_classes)))
        self._class_to_coco_cat_id = dict(zip([c['name'] for c in cats],
                                              self._COCO.getCatIds()))
        self._cat2label = {
            cat_id: i 
            for i, cat_id in enumerate(self._COCO.getCatIds())
        }
        self._image_idx = self._load_image_set_index()
        self._img_infos = self._load_image_info()
        # Extra Augmentation
        self.extra_aug = ExtraAugmentation(**self.mc.extra_aug)
        # Transforms
        self.img_transform = ImageTransform( **self.mc.img_norm_cfg)
        self.bbox_transform = BboxTransform()
        # Default to roidb handler
        self.competition_mode(False)
        self._shuffle_image_idx()

        # Some image sets are "views" (i.e. subsets) into others.
        # For example, minival2014 is a random 5000 image subset of val2014.
        # This mapping tells us where the view's images and proposals come from.
        self._view_map = {
            'minival2014' : 'val2014',          # 5k val2014 subset
            'valminusminival2014' : 'val2014',  # val2014 \setminus minival2014
        }
        coco_name = image_set + year  # e.g., "val2014"
        self._data_name = (self._view_map[coco_name]
                           if coco_name in self._view_map
                           else coco_name)
        # Dataset splits that have ground-truth annotations (test splits
        # do not have gt annotations)
        self._gt_splits = ('train', 'val', 'minival')

    def _load_image_info(self):
        img_infos = []
        for i in self._image_idx:
            info = self._COCO.loadImgs([i])[0]
            info['filename'] = info['file_name']
            img_infos.append(info)
        return img_infos

    def _get_ann_file(self):
        prefix = 'instances' if self._image_set.find('test') == -1 \
                             else 'image_info'
        path = osp.join(self._data_root_path, 'annotations',
                      prefix + '_' + self._image_set + self._year + '.json')
        return path

    def _load_image_set_index(self):
        """
        Load image ids.
        """
        image_ids = self._COCO.getImgIds() 
        return image_ids[:100]

    def _get_widths(self):
        anns = self._COCO.loadImgs(self._image_idx)
        widths = [ann['width'] for ann in anns]
        return widths

    def _image_path_at(self, i):
        """
        Return the absolute path to image i in the image sequence.
        """
        return osp.join(self._img_prefix, self._img_infos[i]['filename'])

    def _get_ann_info(self, idx):
        img_id = self._img_infos[idx]['id']
        ann_ids = self._COCO.getAnnIds(imgIds=[img_id])
        ann_info = self._COCO.loadAnns(ann_ids)
        return self._parse_ann_info(ann_info)

    def _parse_ann_info(self, ann_info, with_mask=False):
        """Parse bbox and mask annotation.
        Args:
            ann_info (list[dict]): Annotation info of an image.
            with_mask (bool): Whether to parse mask annotations.
        Returns:
            dict: A dict containing the following keys: bboxes, bboxes_ignore,
                labels, masks, mask_polys, poly_lens.
        """
        gt_bboxes = []
        gt_labels = []
        gt_bboxes_ignore = []
        # Two formats are provided.
        # 1. mask: a binary map of the same size of the image.
        # 2. polys: each mask consists of one or several polys, each poly is a
        # list of float.
        if with_mask:
            gt_masks = []
            gt_mask_polys = []
            gt_poly_lens = []
        for i, ann in enumerate(ann_info):
            if ann.get('ignore', False):
                continue
            x1, y1, w, h = ann['bbox']
            if ann['area'] <= 0 or w < 1 or h < 1:
                continue
            bbox = [x1, y1, x1 + w - 1, y1 + h - 1]
            if ann['iscrowd']:
                gt_bboxes_ignore.append(bbox)
            else:
                gt_bboxes.append(bbox)
                gt_labels.append(self._cat2label[ann['category_id']])
            if with_mask:
                gt_masks.append(self.coco.annToMask(ann))
                mask_polys = [
                    p for p in ann['segmentation'] if len(p) >= 6
                ]  # valid polygons have >= 3 points (6 coordinates)
                poly_lens = [len(p) for p in mask_polys]
                gt_mask_polys.append(mask_polys)
                gt_poly_lens.extend(poly_lens)
        if gt_bboxes:
            gt_bboxes = np.array(gt_bboxes, dtype=np.float32)
            gt_labels = np.array(gt_labels, dtype=np.int64)
        else:
            gt_bboxes = np.zeros((0, 4), dtype=np.float32)
            gt_labels = np.array([], dtype=np.int64)

        if gt_bboxes_ignore:
            gt_bboxes_ignore = np.array(gt_bboxes_ignore, dtype=np.float32)
        else:
            gt_bboxes_ignore = np.zeros((0, 4), dtype=np.float32)

        ann = dict(
            bboxes=gt_bboxes, labels=gt_labels, bboxes_ignore=gt_bboxes_ignore)

        if with_mask:
            ann['masks'] = gt_masks
            # poly format is not used in the current implementation
            ann['mask_polys'] = gt_mask_polys
            ann['poly_lens'] = gt_poly_lens
        return ann

    def _get_box_file(self, index):
        # first 14 chars / first 22 chars / all chars + .mat
        # COCO_val2014_0/COCO_val2014_000000447/COCO_val2014_000000447991.mat
        file_name = ('COCO_' + self._data_name +
                     '_' + str(index).zfill(12) + '.mat')
        return osp.join(file_name[:14], file_name[:22], file_name)

    def _shuffle_image_idx(self):
        self._perm_idx= np.random.permutation(np.arange(len(self._img_infos)))
        self._cur_idx = 0
        
    def read_image_batch(self, shuffle=True):
        """Only Read a batch of images
        Override imdb method because coco images have different shape
        For Evaluation
        Args:
          shuffle: whether or not to shuffle the dataset
        Returns:
          images: length batch_size list of arrays [height, width, 3]
        """
        mc = self.mc
        if shuffle:
            if self._cur_idx + mc.BATCH_SIZE > len(self._img_infos):
                self._shuffle_image_idx()
            batch_idx = self._perm_idx[self._cur_idx:self._cur_idx + mc.BATCH_SIZE]
            self._cur_idx += mc.BATCH_SIZE
        else:
            if self._cur_idx + mc.BATCH_SIZE > len(self._img_infos):
                batch_idx = np.arange(self_cur_idx, self._cur_idx + mc.BATCH_SIZE) % len(self._img_infos)
                self._cur_idx += mc.BATCH_SIZE - len(self._img_infos)
            else:
                batch_idx = np.arange(self._cur_idx, self._cur_idx + mc.BATCH_SIZE)  
                self._cur_idx += mc.BATCH_SIZE
        images, scales = [], []
        for i in batch_idx:
            im_path = self._image_path_at(i)
            im = cv2.imread(im_path)
            im = im.astype(np.float32, copy=False)
            orig_h, orig_w, _ = [float(v) for v in im.shape]
            
            img_scale = (mc.IMAGE_WIDTH, mc.IMAGE_HEIGHT)
            im, img_shape, pad_shape, scale_factor = self.img_transform(
                im, img_scale, False, keep_ratio=False)

            x_scale = mc.IMAGE_WIDTH/orig_w
            y_scale = mc.IMAGE_HEIGHT/orig_h
            images.append(im)
            scales.append((x_scale, y_scale))

        return images, scales

    def read_batch(self, shuffle=True):
        """Read a batch of image and bounding box annotations.
        For Training
        Args:
          shuffle: whether or not to shuffle the dataset
        Returns:
          image_per_batch: images. Shape: batch_size x width x height x [b, g, r]
          label_per_batch: labels. Shape: batch_size x object_num
          delta_per_batch: bounding box deltas. Shape: batch_size x object_num x
              [dx ,dy, dw, dh]
          aidx_per_batch: index of anchors that are responsible for prediction.
              Shape: batch_size x object_num
          bbox_per_batch: scaled bounding boxes. Shape: batch_size x object_num x
              [cx, cy, w, h]
        """
        mc = self.mc

        if shuffle:
            if self._cur_idx + mc.BATCH_SIZE > len(self._image_idx):
                self._shuffle_image_idx()
            batch_idx = self._perm_idx[self._cur_idx:self._cur_idx + mc.BATCH_SIZE]
            self._cur_idx += mc.BATCH_SIZE
        else:
            if self._cur_idx + mc.BATCH_SIZE > len(self._img_infos):
                batch_idx = np.arange(self_cur_idx, self._cur_idx + mc.BATCH_SIZE) % len(self._img_infos)
                self._cur_idx += mc.BATCH_SIZE - len(self._img_infos)
            else:
                batch_idx = np.arange(self._cur_idx, self._cur_idx + mc.BATCH_SIZE)  
                self._cur_idx += mc.BATCH_SIZE

        image_per_batch = []
        label_per_batch = []
        bbox_per_batch = []
        delta_per_batch = []
        aidx_per_batch = []
        if mc.DEBUG_MODE:
            avg_ious = 0.
            num_objects = 0.
            max_iou = 0.0
            min_iou = 1.0
            num_zero_iou_obj = 0

        for idx in batch_idx:
            # load the image 
            im_path = self._image_path_at(idx)
            im = cv2.imread(im_path)
            im = im.astype(np.float32, copy=False)
            orig_h, orig_w, _ = [float(v) for v in im.shape]

            # load annotations
            ann = self._get_ann_info(idx)
            gt_bboxes = ann['bboxes']
            gt_labels = ann['labels']
            
            #print(gt_labels)
            flip = False
            if mc.DATA_AUGMENTATION:
                im, gt_bboxes, gt_labels = self.extra_aug(im, gt_bboxes, gt_labels)
                flip = True if np.random.rand() < 0.5 else False

            # scale and normalize image
            img_scale = (mc.IMAGE_WIDTH, mc.IMAGE_HEIGHT)
            im, img_shape, pad_shape, scale_factor = self.img_transform(
                im, img_scale, flip, keep_ratio=False)
            image_per_batch.append(im)

            # scale bboxes
            gt_bboxes = self.bbox_transform(gt_bboxes, img_shape, scale_factor,
                                            flip)
            # back to cx, cy, w, h
            gt_bboxes = np.hstack(((gt_bboxes[:, 0:2] + gt_bboxes[:, 2:4]) / 2,
                                    gt_bboxes[:, 2:4] - gt_bboxes[:, 0:2] + 1))

            aidx_per_image, delta_per_image = [], []
            aidx_set = set()
            for i in range(len(gt_bboxes)):
                overlaps = batch_iou(mc.ANCHOR_BOX, gt_bboxes[i])

                aidx = len(mc.ANCHOR_BOX)
                for ov_idx in np.argsort(overlaps)[::-1]:
                    if overlaps[ov_idx] <= 0:
                        if mc.DEBUG_MODE:
                            min_iou = min(overlaps[ov_idx], min_iou)
                            num_objects += 1
                            num_zero_iou_obj += 1
                        break
                    if ov_idx not in aidx_set:
                        aidx_set.add(ov_idx)
                        aidx = ov_idx
                        if mc.DEBUG_MODE:
                            max_iou = max(overlaps[ov_idx], max_iou)
                            min_iou = min(overlaps[ov_idx], min_iou)
                            avg_ious += overlaps[ov_idx]
                            num_objects += 1
                        break

                if aidx == len(mc.ANCHOR_BOX):
                    # even the largeset available overlap is 0, thus, choose one with the
                    # smallest square distance
                    dist = np.sum(np.square(gt_bboxes[i] - mc.ANCHOR_BOX), axis=1)
                    for dist_idx in np.argsort(dist):
                        if dist_idx not in aidx_set:
                            aidx_set.add(dist_idx)
                            aidx = dist_idx
                            break

                box_cx, box_cy, box_w, box_h = gt_bboxes[i]
                delta = [0] * 4
                delta[0] = (box_cx - mc.ANCHOR_BOX[aidx][0]) / mc.ANCHOR_BOX[aidx][2]
                delta[1] = (box_cy - mc.ANCHOR_BOX[aidx][1]) / mc.ANCHOR_BOX[aidx][3]
                delta[2] = np.log(box_w / mc.ANCHOR_BOX[aidx][2])
                delta[3] = np.log(box_h / mc.ANCHOR_BOX[aidx][3])

                aidx_per_image.append(aidx)
                delta_per_image.append(delta)

            label_per_batch.append(gt_labels)
            gt_bboxes = np.array(gt_bboxes, dtype=int)
            bbox_per_batch.append(gt_bboxes)
            delta_per_batch.append(delta_per_image)
            aidx_per_batch.append(aidx_per_image)

            
        if mc.DEBUG_MODE:
            print('max iou: {}'.format(max_iou))
            print('min iou: {}'.format(min_iou))
            print('avg iou: {}'.format(avg_ious / num_objects))
            print('number of objects: {}'.format(num_objects))
            print('number of objects with 0 iou: {}'.format(num_zero_iou_obj))

        return image_per_batch, label_per_batch, delta_per_batch, \
               aidx_per_batch, bbox_per_batch

    def _print_detection_eval_metrics(self, coco_eval):
        IoU_lo_thresh = 0.5
        IoU_hi_thresh = 0.95
        def _get_thr_ind(coco_eval, thr):
            ind = np.where((coco_eval.params.iouThrs > thr - 1e-5) &
                           (coco_eval.params.iouThrs < thr + 1e-5))[0][0]
            iou_thr = coco_eval.params.iouThrs[ind]
            assert np.isclose(iou_thr, thr)
            return ind

        ind_lo = _get_thr_ind(coco_eval, IoU_lo_thresh)
        ind_hi = _get_thr_ind(coco_eval, IoU_hi_thresh)
        # precision has dims (iou, recall, cls, area range, max dets)
        # area range index 0: all area ranges
        # max dets index 2: 100 per image
        precision = \
            coco_eval.eval['precision'][ind_lo:(ind_hi + 1), :, :, 0, 2]
        ap_default = np.mean(precision[precision > -1])
        print ('~~~~ Mean and per-category AP @ IoU=[{:.2f},{:.2f}] ~~~~'.format(
                IoU_lo_thresh, IoU_hi_thresh))
        print('{:.1f}'.format(100 * ap_default))
        for cls_ind, cls in enumerate(self._classes):
            precision = coco_eval.eval['precision'][ind_lo:(ind_hi + 1), :, cls_ind, 0, 2]
            ap = np.mean(precision[precision > -1])
            print('{:.1f}'.format(100 * ap))

        print('~~~~ Summary metrics ~~~~')
        coco_eval.summarize()

    def _do_detection_eval(self, res_file, output_dir):
        ann_type = 'bbox'
        coco_dt = self._COCO.loadRes(res_file)
        coco_eval = COCOeval(self._COCO, coco_dt)
        coco_eval.params.useSegm = (ann_type == 'segm')
        coco_eval.evaluate()
        coco_eval.accumulate()
        self._print_detection_eval_metrics(coco_eval)
        eval_file = osp.join(output_dir, 'detection_results.pkl')
        with open(eval_file, 'wb') as fid:
            cPickle.dump(coco_eval, fid, cPickle.HIGHEST_PROTOCOL)
        print('Wrote COCO eval results to: {}'.format(eval_file))

    def _coco_results_one_category(self, boxes, cat_id):
        results = []
        for im_ind, index in enumerate(self.image_idx):
            dets = np.array(boxes[im_ind], dtype=np.float)
            if len(dets) == 0:
                continue
            scores = dets[:, -1]
            xs = dets[:, 0]
            ys = dets[:, 1]
            ws = dets[:, 2] - xs + 1
            hs = dets[:, 3] - ys + 1
            results.extend(
              [{'image_id' : index,
                'category_id' : cat_id,
                'bbox' : [xs[k], ys[k], ws[k], hs[k]],
                'score' : scores[k]} for k in range(dets.shape[0])])
        return results

    def _write_coco_results_file(self, all_boxes, res_file):
        # [{"image_id": 42,
        #   "category_id": 18,
        #   "bbox": [258.15,41.29,348.26,243.78],
        #   "score": 0.236}, ...]
        results = []
        for cls_ind, cls in enumerate(self._classes):
            print('Collecting {} results ({:d}/{:d})'.format(cls, cls_ind,
                                                          self.num_classes))
            coco_cat_id = self._class_to_coco_cat_id[cls]
            results.extend(self._coco_results_one_category(all_boxes[cls_ind],
                                                           coco_cat_id))
        print('Writing results json to {}'.format(res_file))
        with open(res_file, 'w') as fid:
            json.dump(results, fid)

    def evaluate_detections(self, eval_dir, global_step, all_boxes):
        det_file_dir = osp.join(
            eval_dir, 'detection_files_{:s}'.format(global_step))
        if not osp.isdir(det_file_dir):
            os.makedirs(det_file_dir)

        res_file = osp.join(det_file_dir, ('detections_' +
                                         self._image_set +
                                         self._year +
                                         '_results'))
        if self.config['use_salt']:
            res_file += '_{}'.format(str(uuid.uuid4()))
        res_file += '.json'
        self._write_coco_results_file(all_boxes, res_file)
        # Only do evaluation on non-test sets
        if self._image_set.find('test') == -1:
            self._do_detection_eval(res_file, det_file_dir)
        # Optionally cleanup results json file
        if self.config['cleanup']:
            os.remove(res_file)

    def do_detection_analysis_in_eval(self, eval_dir, global_step):
        # Ignored for COCO
        return (0, 0)

    def competition_mode(self, on):
        if on:
            self.config['use_salt'] = False
            self.config['cleanup'] = False
        else:
            self.config['use_salt'] = True
            self.config['cleanup'] = True


if __name__ == "__main__":
    # Test coco
    from src.config import coco_squeezeDet_config
    mc = coco_squeezeDet_config()
    mc.IS_TRAINING = True
    
    #path = '/mnt/ssd2/od/coco'
    #path = 'C:\\Users\\phili\\Documents\\aUToronto\\squeezeDet\\data\\mapillary'
    print("testing coco driver")
    imdb = coco('train', mc.data_root, mc)
    print("Testing Get Image Info")
    print(imdb._img_infos)
    print("Testing get image path")
    print(imdb._image_path_at(1))
    print("Testing get annotations")
    print(imdb._get_ann_info(0))

    print("Testing read batch")
    ans = imdb.read_batch()
