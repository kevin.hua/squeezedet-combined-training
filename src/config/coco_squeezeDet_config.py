
"""Model configuration for pascal dataset"""

import numpy as np
import os.path as osp

from .config import base_model_config

def coco_squeezeDet_config():
  """Specify the parameters to tune below."""
  mc                       = base_model_config('COCO')
  mc.DEBUG_MODE            = False

  #mc.data_root = 'C:\\Users\\phili\\Documents\\aUToronto\\squeezeDet\\data\\mapillary'
  mc.data_root='/mnt/ssd2/od/coco'
  mc.train_prefix = osp.join(mc.data_root, 'images', 'train2017')
  mc.val_prefix = osp.join(mc.data_root, 'images', 'val2017')
  
  mc.NUM_THREAD = 2
  mc.QUEUE_CAPACITY = 50
 
  mc.IMAGE_WIDTH           = 736
  mc.IMAGE_HEIGHT          = 672
  mc.BATCH_SIZE            = 16

  mc.WEIGHT_DECAY          = 0.0001
  mc.LEARNING_RATE         = 0.02
  mc.DECAY_STEPS           = 80000
  mc.MAX_GRAD_NORM         = 1.0
  mc.MOMENTUM              = 0.9
  mc.LR_DECAY_FACTOR       = 0.5

  mc.LOSS_COEF_BBOX        = 5.0
  mc.LOSS_COEF_CONF_POS    = 75.0
  mc.LOSS_COEF_CONF_NEG    = 100.0
  mc.LOSS_COEF_CLASS       = 1.0

  mc.PLOT_PROB_THRESH      = 0.1
  mc.NMS_THRESH            = 0.4
  mc.PROB_THRESH           = 0.005
  mc.TOP_N_DETECTION       = 64

  mc.DATA_AUGMENTATION     = False
  mc.DRIFT_X               = 150
  mc.DRIFT_Y               = 100
  mc.EXCLUDE_HARD_EXAMPLES = False

  mc.BGR_MEANS = np.array([[[103.53, 116.28, 123.675]]])
  mc.img_norm_cfg = dict(mean=mc.BGR_MEANS, std=[1, 1, 1], to_rgb=False)
  mc.extra_aug = dict(
                photo_metric_distortion=dict(
                    brightness_delta=32,
                    contrast_range=(0.5, 1.5),
                    saturation_range=(0.5, 1.5),
                    hue_delta=18),
                expand=dict(
                    mean=mc.img_norm_cfg['mean'],
                    to_rgb=mc.img_norm_cfg['to_rgb'],
                    ratio_range=(1, 4)),
                random_crop=dict(
                    min_ious=(0.1, 0.3, 0.5, 0.7, 0.9), min_crop_size=0.3))

  mc.ANCHOR_BOX            = set_anchors(mc)
  mc.ANCHORS               = len(mc.ANCHOR_BOX)
  mc.ANCHOR_PER_GRID       = 9

  return mc


def set_anchors(mc):
  H, W, B = 42, 46, 9
  anchor_shapes = np.reshape(
      [np.array(
          [[  36.,  37.], [ 366., 174.], [ 115.,  59.],
           [ 162.,  87.], [  38.,  90.], [ 258., 173.],
           [ 224., 108.], [  78., 170.], [  72.,  43.]])] * H * W,
      (H, W, B, 2)
  )
  center_x = np.reshape(
      np.transpose(
          np.reshape(
              np.array([np.arange(1, W+1)*float(mc.IMAGE_WIDTH)/(W+1)]*H*B),
              (B, H, W)
          ),
          (1, 2, 0)
      ),
      (H, W, B, 1)
  )
  center_y = np.reshape(
      np.transpose(
          np.reshape(
              np.array([np.arange(1, H+1)*float(mc.IMAGE_HEIGHT)/(H+1)]*W*B),
              (B, W, H)
          ),
          (2, 1, 0)
      ),
      (H, W, B, 1)
  )
  anchors = np.reshape(
      np.concatenate((center_x, center_y, anchor_shapes), axis=3),
      (-1, 4)
  )

  return anchors


if __name__ == "__main__":

    mc = coco_squeezeDet_config()
    anchors = set_anchors(mc)
