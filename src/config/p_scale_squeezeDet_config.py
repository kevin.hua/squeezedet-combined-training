# Author: Bichen Wu (bichen@berkeley.edu) 08/25/2016

"""Model configuration for pascal dataset"""

import numpy as np

from .config import base_model_config

def p_scale_squeezeDet_config():
  """Specify the parameters to tune below."""
  mc                       = base_model_config('KITTI')
  
  mc.IMG_SUFFIX            = '.jpg'
  mc.IMAGE_WIDTH           = 1952
  mc.IMAGE_HEIGHT          = 560
  mc.BATCH_SIZE            = 10
  mc.TOP_CROP              = 900    # How far from top of image down does desired image start
  mc.INPUT_HEIGHT          = 700    # How tall is the cropped area that we are keeping (after TOP_CROP)
  mc.CROP_STUFF            = True

  mc.NUM_THREADS           = 4
  mc.IMAGE_RATIO           = mc.IMAGE_HEIGHT/mc.IMAGE_WIDTH
  mc.NUM_THREADS           = 4
  mc.OPTIMIZER             = "Adam"
  mc.BETA1                 = 0.9
  mc.BETA2                 = 0.999
  mc.EPSILON               = 0.1

  mc.ADAM                  = True 
  mc.WEIGHT_DECAY          = 0.0001
  mc.LEARNING_RATE         = 0.001
  mc.DECAY_STEPS           = 2500
  mc.MAX_GRAD_NORM         = 1.0
  mc.MOMENTUM              = 0.9
  mc.LR_DECAY_FACTOR       = 0.5

  mc.LOSS_COEF_BBOX        = 5.0
  mc.LOSS_COEF_CONF_POS    = 75.0
  mc.LOSS_COEF_CONF_NEG    = 100.0
  mc.LOSS_COEF_CLASS       = 1.0

  mc.PLOT_PROB_THRESH      = 0.4
  mc.NMS_THRESH            = 0.4
  mc.PROB_THRESH           = 0.005
  mc.TOP_N_DETECTION       = 64

  mc.DATA_AUGMENTATION     = False    # This is normally set to True, set False for now
  mc.DRIFT_X               = 150
  mc.DRIFT_Y               = 100
  mc.EXCLUDE_HARD_EXAMPLES = False
  mc.MIN_HEIGHT            = 0.058

  mc.ANCHOR_BOX            = set_anchors(mc)
  mc.ANCHORS               = len(mc.ANCHOR_BOX)
  mc.ANCHOR_PER_GRID       = 9

  return mc

def set_anchors(mc):
  H, W, B = 35, 122, 9        # H,W here should be 0.0625 * IMAGE_HEIGHT, IMAGE_WIDTH
  anchor_shapes = np.reshape(
      [np.array(
          [[  33., 64. ],     # These anchors were obtained by running kmeans
           [ 412., 257.],     # clustering on our ROS data
           [ 18.,  22. ],
           [ 112., 238.], 
           [ 83.,  36. ], 
           [ 204., 96. ],
           [ 450., 578.], 
           [ 200., 367.], 
           [ 66.,  132.]])] * H * W,
      (H, W, B, 2)
  )
  center_x = np.reshape(
      np.transpose(
          np.reshape(
              np.array([np.arange(1, W+1)*float(mc.IMAGE_WIDTH)/(W+1)]*H*B), 
              (B, H, W)
          ),
          (1, 2, 0)
      ),
      (H, W, B, 1)
  )
  center_y = np.reshape(
      np.transpose(
          np.reshape(
              np.array([np.arange(1, H+1)*float(mc.IMAGE_HEIGHT)/(H+1)]*W*B),
              (B, W, H)
          ),
          (2, 1, 0)
      ),
      (H, W, B, 1)
  )
  anchors = np.reshape(
      np.concatenate((center_x, center_y, anchor_shapes), axis=3),
      (-1, 4)
  )

  return anchors
