# Author: Bichen Wu (bichen@berkeley.edu) 08/25/2016

"""Evaluation"""


import sys
import os
BAD_CV='/opt/ros/kinetic/lib/python2.7/dist-packages'
if BAD_CV in sys.path:
    sys.path.remove(BAD_CV)
if os.getcwd() not in sys.path:
    sys.path.append(os.getcwd())
import cv2
from datetime import datetime
import time

import numpy as np
from six.moves import xrange
import tensorflow as tf
import pickle
from pprint import pprint

from config import *
from dataset import pascal_voc, kitti, coco, combined
from utils.util import bbox_transform, Timer, batch_iou_xy
from nets import *

from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.patches as patches


FLAGS = tf.app.flags.FLAGS

tf.app.flags.DEFINE_string('dataset', 'COMBINED',
                           """Currently support PASCAL_VOC or KITTI or COCO dataset.""")
tf.app.flags.DEFINE_string('data_path', '/mnt/ssd2/od/SCALE/', """Root directory of data""")
tf.app.flags.DEFINE_string('image_set', 'val',
                           """Only used for VOC data."""
                           """Can be train, trainval, val, or test""")
tf.app.flags.DEFINE_string('eval_dir', '/mnt/ssd2/od/kevin/eval_results',
                            """Directory where to write event logs """)
tf.app.flags.DEFINE_string('checkpoint_path', '/mnt/ssd2/od/kevin/log_combined/old_anchor_boxes/train/',
#tf.app.flags.DEFINE_string('checkpoint_path', '/mnt/ssd2/od/kevin/checkpoints/scale_min_height/model.ckpt-1000',
                             """Path to the training checkpoint.""")
tf.app.flags.DEFINE_boolean('run_once', False,
                             """Whether to run eval only once.""")
tf.app.flags.DEFINE_boolean('p', False, """use p or not""")

tf.app.flags.DEFINE_string('net', 'squeezeDet',
                           """Neural net architecture.""")
tf.app.flags.DEFINE_string('visualize', False,  """Visualize Evaluations.""")
tf.app.flags.DEFINE_string('gpu', '1', """gpu id.""")
tf.app.flags.DEFINE_integer('eval_interval_secs', 120, """intervals""")
def draw_bbox(ax, bbox, text, bcolor, ls='-'):
    rect = patches.Rectangle((bbox[0], bbox[1]), bbox[2], bbox[3], linestyle=ls,linewidth=1,edgecolor=bcolor,facecolor='none')
    plt.text(bbox[0], bbox[1], text, color=bcolor)
    ax.add_patch(rect)
    return

def get_cm(det_bboxes, det_classes, det_score, gt_bboxes, gt_classes, iou_thresh, visualize=False, img=None, ax=None):
    
    if visualize:
        assert img is not None and ax is not None, "no input image"
        # Visualization

        plt.cla()
        ax.imshow(img)
        #ax.set_title(img_file)

    order = np.argsort(1-det_score, axis=0)
    det_bboxes = det_bboxes[order]
    iou = np.array([batch_iou_xy(det_bboxes, b) for b in gt_bboxes])
    # pprint(iou)
    
    det_matched = []
    gt_matched = []

    matches_ind = []
    match_iou = []
    shape = np.shape(iou)
    TP = 0
    TN = 0
    FP = 0
    FN = 0
    while np.amax(iou) >= iou_thresh:
        max_inds = np.unravel_index(np.argmax(iou), shape)
        matches_ind.append(max_inds)
        match_iou.append(iou[max_inds[0], max_inds[1]])

        if visualize:
            det_matched.append(max_inds[1])
            gt_matched.append(max_inds[0])

            draw_bbox(ax, gt_bboxes[max_inds[0]], "TP", "b", '-')
            draw_bbox(ax, det_bboxes[max_inds[1]], "TP", "c", '-.')

        TP += 1
        iou[max_inds[0], :] = 0
        iou[:, max_inds[1]] = 0
        
    if visualize:
        for i in range(len(gt_bboxes)):
            if i in gt_matched:
                continue
            draw_bbox(ax, gt_bboxes[i], "FN", "y", '-')

        for i in range(len(det_bboxes)):
            if i in det_matched:
                continue
            draw_bbox(ax, det_bboxes[i], "FP", "r", '-.')

    FP = len(det_bboxes) - TP
    FN = len(gt_bboxes) - TP
    if len(match_iou) == 0:
      mean_iou = 0
    else:
      mean_iou = np.mean(match_iou)
    
    if visualize:
        print(TP, TN, FP, FN, mean_iou)
        key = input("Press any key to continue")

    return TP, TN, FP, FN, mean_iou

def eval_once(saver, ckpt_path, imdb, model, mc):

  with tf.Session(config=tf.ConfigProto(allow_soft_placement=True)) as sess:

    # Restores from checkpoint
    saver.restore(sess, ckpt_path)
    # Assuming model_checkpoint_path looks something like:
    #   /ckpt_dir/model.ckpt-0,
    # extract global_step from it.
    global_step = ckpt_path.split('/')[-1].split('-')[-1]

    num_images = len(imdb.image_idx)
    TP = np.zeros(num_images)
    TN = np.zeros(num_images)
    FP = np.zeros(num_images)
    FN = np.zeros(num_images)
    mean_iou = np.zeros(num_images)

    if FLAGS.visualize:
        fig,ax = plt.subplots(1)
        plt.ion()
        plt.show()

    _t = {'im_detect': Timer(), 'im_read': Timer(), 'misc': Timer()}

    for i in xrange(num_images):
      idx = imdb._image_idx[i]

      # Read Images
      _t['im_read'].tic()
      o_im = cv2.imread(imdb._image_path_at(idx))
      im = (o_im - mc.BGR_MEANS).astype(np.float32)
      orig_h, orig_w, _ = [float(v) for v in im.shape]
      _t['im_read'].toc()

      _t['misc'].tic()
      # Load annotations
      orig_classes = ([b[4] for b in imdb._rois[idx]])
      orig_bbox = np.array([[b[0]-b[2]/2, b[1]-b[3]/2, b[2], b[3]] for b in imdb._rois[idx]])
     
      # CROP IMAGE ######################
      if abs((orig_h/orig_w) - mc.IMAGE_RATIO) > 0.2:
        #print("Cropping image")
        #print(orig_h/orig_w)
        new_h = int(orig_w*mc.IMAGE_RATIO)
        top_crop = int((orig_h - new_h)/2)
        bot_crop = top_crop + new_h
        #print(top_crop)
        #print(bot_crop)
        #print(orig_h)
        #print(orig_w)
        #print(mc.IMAGE_RATIO)
        #print(orig_bbox)
        o_im = o_im[top_crop:bot_crop, :, :]
        im = im[top_crop:bot_crop, :, :]

        gt_bboxes = []
        for j in range(len(orig_bbox)):
          if orig_classes[j] == 0:
            continue
          b = orig_bbox[j]
          y_min = b[1]
          y_max = b[3] + b[1]
          if y_max < top_crop or y_min > bot_crop:  # BB is outside cropped area
            continue
          gt_bboxes.append([b[0], max(y_min-top_crop, 0), b[2], min(y_max-top_crop, new_h)-max(y_min-top_crop, 0)])
        gt_bboxes = np.array(gt_bboxes)
      else:
        print("Not Cropping")
        gt_bboxes = orig_bbox
      gt_bboxes = [b for b in gt_bboxes if not np.any(b[2:] < 20)]
      gt_classes = np.ones(len(gt_bboxes))
      orig_h, orig_w, _ = [float(v) for v in im.shape]
      im = cv2.resize(im, (mc.IMAGE_WIDTH, mc.IMAGE_HEIGHT))
      #o_im = cv2.resize(o_im, (mc.IMAGE_WIDTH, mc.IMAGE_HEIGHT))
      x_scale = mc.IMAGE_WIDTH/orig_w
      y_scale = mc.IMAGE_HEIGHT/orig_h
      _t['misc'].toc()
     
      _t['im_detect'].tic()
      det_b, det_p, det_c = sess.run(
          [model.det_boxes, model.det_probs, model.det_class],
          feed_dict={model.image_input:[im]})
      _t['im_detect'].toc()

      #gt_bboxes[:, 0::2] *= x_scale
      #gt_bboxes[:, 1::2] *= y_scale

      det_bboxes, det_scores, det_classes = model.filter_prediction(
            det_b[0], det_p[0], det_c[0])
      det_bboxes = np.array(det_bboxes)
      det_scores = np.array(det_scores)
      det_classes = np.array(det_classes)
      det_bboxes[:, 0::2] /= x_scale
      det_bboxes[:, 1::2] /= y_scale
      det_bboxes[:, 0:2] -= det_bboxes[:, 2:]/2
      inds = np.not_equal(det_classes, 0)* np.greater(det_scores, 0.6)
      # Remove non pedestrian or cyclist classes
      # Currently we treat pedestrian and cyclist the same
      det_bboxes = det_bboxes[inds]
      det_scores = det_scores[inds]
      det_classes = det_classes[inds]
      #det_bboxes = np.take(det_bboxes, inds, axis=0)[0]
      #det_scores = np.take(det_scores, inds)[0]
      #det_classes = np.take(det_classes, inds)[0]
      #print(np.shape(det_bboxes))
      #print(np.shape(det_classes))
      #print(det_bboxes)
      #print(gt_bboxes)

      if len(det_bboxes) == 0:
        FN[i] = len(gt_bboxes)
        continue
      elif len(gt_bboxes) == 0:
        FP[i] = len(det_bboxes)
        continue

      if FLAGS.visualize:
        TP[i], TN[i], FP[i], FN[i], mean_iou[i] = get_cm(det_bboxes, det_classes, det_scores, gt_bboxes, gt_classes, 0.5, 
              True, cv2.cvtColor(o_im, cv2.COLOR_BGR2RGB), ax)
      else:
        TP[i], TN[i], FP[i], FN[i], mean_iou[i] = get_cm(det_bboxes, det_classes, det_scores, gt_bboxes, gt_classes, 0.5)
      

      print ('im_detect: {:d}/{:d} im_read: {:.3f}s '
             'detect: {:.3f}s misc: {:.3f}s'.format(
                i+1, num_images, _t['im_read'].average_time,
                _t['im_detect'].average_time, _t['misc'].average_time))

    # Save Detection Results
    pickle_save_path = os.path.join(FLAGS.eval_dir, FLAGS.dataset+"_"+global_step+"_CM.pickle")
    #txt_save_path = os.path.join(FLAGS.eval_dir, FLAGS.dataset+"_"+global_step+"_summary.txt")
    txt_save_path = os.path.join(FLAGS.eval_dir, FLAGS.dataset+"_summary.txt")
    
    lines = []
    lines.append("======== EVALUATION RESULTS ========")
    lines.append(str(datetime.now()))
    lines.append(ckpt_path)
    lines.append(FLAGS.data_path)
    lines.append(FLAGS.image_set)
    lines.append(" ")
    lines.append("True Positives:   "+str(np.sum(TP)))
    lines.append("True Negatives:   "+str(np.sum(TN)))
    lines.append("False Positives:  "+str(np.sum(FP)))
    lines.append("False Negatives:  "+str(np.sum(FN)))
    lines.append(" ")
    lines.append("Precision:    "+str(np.sum(TP)/np.sum(TP+FP)))
    lines.append("Recall:       "+str(np.sum(TP)/np.sum(TP+FN)))
    lines.append("Mean IOU:     "+str(np.nanmean(mean_iou)))

    pprint(lines)

    with open(txt_save_path, 'a+') as f:
      #f.writelines(lines)
      f.write('\n'.join(lines) + '\n')
    f.close()

    


def evaluate():
  """Evaluate."""
  # assert FLAGS.dataset in ['KITTI', 'SCALE', 'COCO', 'COMBINED'], \
  assert FLAGS.dataset in ['KITTI', 'COMBINED'], \
      'Dataset not supported'

  os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu

  with tf.Graph().as_default() as g:
    if FLAGS.dataset == 'KITTI':
      assert FLAGS.net == 'vgg16' or FLAGS.net == 'resnet50' \
          or FLAGS.net == 'squeezeDet' or FLAGS.net == 'squeezeDet+', \
          'Selected neural net architecture not supported: {}'.format(FLAGS.net)
      if FLAGS.net == 'vgg16':
        mc = kitti_vgg16_config()
        mc.BATCH_SIZE = 1 # TODO(bichen): allow batch size > 1
        mc.LOAD_PRETRAINED_MODEL = False
        model = VGG16ConvDet(mc)
      elif FLAGS.net == 'resnet50':
        mc = kitti_res50_config()
        mc.BATCH_SIZE = 1 # TODO(bichen): allow batch size > 1
        mc.LOAD_PRETRAINED_MODEL = False
        model = ResNet50ConvDet(mc)
      elif FLAGS.net == 'squeezeDet':
        mc = kitti_squeezeDet_config()
        mc.BATCH_SIZE = 1 # TODO(bichen): allow batch size > 1
        mc.LOAD_PRETRAINED_MODEL = False
        model = SqueezeDet(mc)
      elif FLAGS.net == 'squeezeDet+':
        mc = kitti_squeezeDetPlus_config()
        mc.BATCH_SIZE = 1 # TODO(bichen): allow batch size > 1
        mc.LOAD_PRETRAINED_MODEL = False
        model = SqueezeDetPlus(mc)

      mc.IMAGE_SUFFIX = ".png"
      imdb = kitti(FLAGS.image_set, FLAGS.data_path, mc)
    elif FLAGS.dataset == 'COMBINED':        
      if FLAGS.p:
        mc = p_scale_squeezeDet_config()
      else:
        mc = scale_squeezeDet_config()
      mc.BATCH_SIZE = 1 # TODO(bichen): allow batch size > 1
      mc.LOAD_PRETRAINED_MODEL = False
      #mc.IMAGE_SUFFIX = ".jpg"
      mc.NUM_THREAD = 4
      model = SqueezeDet(mc)
      imdb = combined(FLAGS.image_set, [FLAGS.data_path], mc)

    saver = tf.train.Saver(model.model_params)

    if FLAGS.run_once:
      # When run_once is true, checkpoint_path should point to the exact
      if os.path.isdir(FLAGS.checkpoint_path):
        ckpt = tf.train.latest_checkpoint(FLAGS.checkpoint_path)
      else:
        ckpt = FLAGS.checkpoint_path
      # print(ckpt)
      # exit()
      eval_once( saver, ckpt, imdb, model, mc)
      return
    else:
      ckpts = set() 
      while True:
        ckpt = tf.train.get_checkpoint_state(FLAGS.checkpoint_path)
        if ckpt and ckpt.model_checkpoint_path:
          if ckpt.model_checkpoint_path in ckpts:
            # Do not evaluate on the same checkpoint
            print ('Wait {:d}s for new checkpoints to be saved ... '
                      .format(FLAGS.eval_interval_secs))
            time.sleep(FLAGS.eval_interval_secs)
          else:
            ckpts.add(ckpt.model_checkpoint_path)
            print ('Evaluating {}...'.format(ckpt.model_checkpoint_path))
            eval_once(saver, ckpt.model_checkpoint_path, imdb, model, mc)
        else:
          print('No checkpoint file found')
          if not FLAGS.run_once:
            print ('Wait {:d}s for new checkpoints to be saved ... '
                      .format(FLAGS.eval_interval_secs))
            time.sleep(FLAGS.eval_interval_secs)


def main(argv=None):  # pylint: disable=unused-argument
  if not tf.gfile.Exists(FLAGS.eval_dir):
    # tf.gfile.DeleteRecursively(FLAGS.eval_dir)
    tf.gfile.MakeDirs(FLAGS.eval_dir) 
  evaluate()


if __name__ == '__main__':
  tf.app.run()
